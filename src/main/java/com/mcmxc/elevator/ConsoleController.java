package com.mcmxc.elevator;

import lombok.Data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.LinkedBlockingQueue;

@Data
public class ConsoleController implements Runnable {

    private int maxFloor;

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private LinkedBlockingQueue<Integer> queueFloors;


    @Override
    public void run() {
        boolean isProcessed = true;
        do {
            System.out.println("To select floor from elevator enter \"1\"");
            System.out.println("To call elevator from floor enter \"2\"");
            System.out.println("To stop simulator enter \"0\" (It will stop when queue will be empty)");
            int command = takeInputCommand("Select type: ");
            if (command == 0) {
                isProcessed = false;
                queueFloors.offer(command);
            } else {
                command = takeInputCommand("Select floor: ");
                if (!queueFloors.contains(command)) {
                    queueFloors.offer(command);
                } else {
                    System.out.println("This floor is already chosen");
                }
            }

        } while (isProcessed);
    }


    private Integer takeInputCommand(String s) {
        boolean correctInput = false;
        int floor = -1;
        System.out.println(s);
        do {
            try {
                String line = reader.readLine();
                try {
                    floor = Integer.parseInt(line);
                } catch (NumberFormatException e) {
                    System.out.println("Incorrect number");
                    continue;
                }
                if (floor >= 0 && floor <= maxFloor) {
                    correctInput = true;
                } else {
                    System.out.println("Floor doesn't exist");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (!correctInput);
        return floor;
    }
}

