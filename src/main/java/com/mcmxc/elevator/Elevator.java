package com.mcmxc.elevator;

import com.mcmxc.elevator.Util.ElevatorDirection;
import com.mcmxc.elevator.Util.TimerUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

@Data
public class Elevator implements Runnable {

    private Integer currentFloor;
    private Integer maxFloor;
    private ElevatorDirection currentDirection;
    private boolean notFinished;
    private LinkedBlockingQueue<Integer> queueFloors;

    public Elevator(int currentFloor, int maxFloor) {
        this.currentFloor = currentFloor;
        this.maxFloor = maxFloor;
        currentDirection = ElevatorDirection.STAY;
        queueFloors = new LinkedBlockingQueue<>();
        notFinished = true;
    }


    @Override
    public void run() {
        do {
            if (!queueFloors.isEmpty()) {
                move(queueFloors.peek());
            }
        } while (notFinished);
    }

    private void checkQueue(int nextFloor) {
        List<Integer> floorsBeforeNextFloorList = new ArrayList<>();
        List<Integer> list = new ArrayList<>(queueFloors);
        for (Integer integer : queueFloors) {
            if (integer > currentFloor && integer < nextFloor) {
                floorsBeforeNextFloorList.add(integer);

            } else if (integer < currentFloor && integer > nextFloor) {
                floorsBeforeNextFloorList.add(integer);
            }
        }

        if (floorsBeforeNextFloorList.size() != 0) {
            if (currentFloor < queueFloors.peek()) {

                Collections.sort(floorsBeforeNextFloorList);
                list.removeAll(floorsBeforeNextFloorList);
                list.addAll(0, floorsBeforeNextFloorList);
                queueFloors.removeAll(list);
                for (int i = 0; i < list.size(); i++) {
                    queueFloors.offer(list.get(i));
                }
            } else if (currentFloor > queueFloors.peek()) {

                Collections.sort(floorsBeforeNextFloorList);
                Collections.reverse(floorsBeforeNextFloorList);
                list.removeAll(floorsBeforeNextFloorList);
                list.addAll(0, floorsBeforeNextFloorList);
                queueFloors.removeAll(list);

                queueFloors.addAll(list);

            }
        }
    }


    private void printCurrentFloor(int currentFloor) {
        System.out.print(String.format("Current floor: %d", currentFloor));
        System.out.println(TimerUtil.getCurrentTime());
    }

    private void move(Integer nextFloor) {
        if (nextFloor == 0) {
            notFinished = false;
            queueFloors.clear();

        } else {

            if (nextFloor == currentFloor) {
                currentDirection = ElevatorDirection.STAY;
                turnDoor();
                queueFloors.remove(currentFloor);
            } else if (nextFloor > currentFloor) {
                currentDirection = ElevatorDirection.UP;
                moveNextFloor(nextFloor);
            } else if (nextFloor < currentFloor) {
                currentDirection = ElevatorDirection.DOWN;
                moveNextFloor(nextFloor);
            }
        }
    }


    private void moveNextFloor(int nextFloor) {
        if (currentDirection != ElevatorDirection.STAY) {

            do {

                printCurrentFloor(currentFloor);
                TimerUtil.waitProcess(10000);
                currentFloor += currentDirection.getDirection();
                checkQueue(nextFloor);


            } while (currentFloor != queueFloors.peek());
            queueFloors.poll();
            turnDoor();

        }
    }


    private void turnDoor() {
        printCurrentFloor(currentFloor);
        TimerUtil.waitProcess(2000);
        System.out.print("Opening doors   ");
        System.out.println(TimerUtil.getCurrentTime());
        TimerUtil.waitProcess(2000);
        System.out.print("Closing doors   ");
        System.out.println(TimerUtil.getCurrentTime());

    }


}
