package com.mcmxc.elevator;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.LinkedBlockingQueue;

@Service
public class ElevatorService {

    @Autowired
    private Elevator elevator;

    @Autowired
    private ConsoleController consoleController;

    private LinkedBlockingQueue<Integer> queueFloors;


    public void startSimulation() {
        queueFloors=new LinkedBlockingQueue<>();

        consoleController.setMaxFloor(elevator.getMaxFloor());
        consoleController.setQueueFloors(queueFloors);
        elevator.setQueueFloors(queueFloors);
        new Thread(elevator).start();
        new Thread(consoleController).start();
    }
}
