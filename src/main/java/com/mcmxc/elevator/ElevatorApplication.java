package com.mcmxc.elevator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ElevatorApplication implements CommandLineRunner {

    @Autowired
    ElevatorService elevatorService;

    @Bean
    public Elevator getElevator() {
        return new Elevator(1, 7);
    }

    @Bean
    public ConsoleController getConsoleController() {
        return new ConsoleController();
    }


    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ElevatorApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        elevatorService.startSimulation();
    }
}
