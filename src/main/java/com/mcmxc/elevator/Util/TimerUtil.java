package com.mcmxc.elevator.Util;

import java.text.SimpleDateFormat;
import java.util.Date;


public class TimerUtil {

    public static void waitProcess(final long waitTime) {
        long startTime = System.currentTimeMillis();
        do {
        } while (System.currentTimeMillis() - startTime < waitTime);
    }


    public static String getCurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return "  *********  " + dateFormat.format(new Date());
    }


}
