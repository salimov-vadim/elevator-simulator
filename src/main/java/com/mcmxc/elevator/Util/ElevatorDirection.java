package com.mcmxc.elevator.Util;

public enum ElevatorDirection {
    UP(1),
    DOWN(-1),
    STAY(0);

    int direction;

    ElevatorDirection(int direction) {
        this.direction = direction;
    }

    public int getDirection() {
        return direction;
    }
}
